class NegociacaoController {

    constructor() {

        // Ao jogar uma função que internamente usa o 'this'
        // dentro de $, o this deixa de ser o document
        // para tratar isso usamos o bind(document)
        let $ = document.querySelector.bind(document)

        this._data = $("#data");
        this._quantidade = $("#quantidade");
        this._valor = $("#valor");



        this._listaNegociacao = ProxyFactory.create(
            new ListaNegociacao(),

            // Registrando função callback, ela será chamada em ProxyFactory (return callBackFunction(target);_)
            (listaNegociacao) => {
                this._updateNegociacaoTableView(listaNegociacao);
            },

            // Definindo funções que dispararão a função callback definida abaixo. 
            // Em formato de varargs
            'adicionaNegociacao',
            'esvazia'

        );


        this._negociacaoTableView = new NegociacaoTableView($("#negociacaoTableView"));

        this._mensagem = ProxyFactory.create(new Mensagem(),

            (mensagem) => {
                this._exibeMensagem(mensagem.texto)
            },

            // Varargs dos atributos
            'texto'

        );

        this._mensagemView = new MensagemView($("#messageView"));
    }

    add(event) {

        event.preventDefault();

        let negociacao = this._criaNegociacao();

        // Obtendo a conexão com o banco do navegador.
        ConnectionFactory.getConnection()
            .then((conn) => {

                // Obtendo o Dao de acessoa ao banco
                let negociacaoDao = new NegociacaoDAO(conn);

                negociacaoDao
                    .adiciona(negociacao)
                    .then(() => {

                        // Dispara a função GET dentro da Proxy.
                        this._listaNegociacao.adicionaNegociacao(negociacao);

                        // Dispara a função SET dentro da Proxy.
                        this._mensagem.texto = "Negociação adicionado com Sucesso.";

                        this._limpaFormulario();
                    })

                    .catch((error) => {
                        this._mensagem.texto = error;
                    });
            })
            .catch((error) => {
                this._mensagem.texto = error;
            });
    }

    clearTable() {

        // Dispara a função GET dentro da Proxy.
        this._listaNegociacao.esvazia();

        this._focusForm();

        // Dispara a função SET dentro da Proxy.
        this._mensagem.texto = "Lista de negociações apagada com sucesso.";
    }

    importaNegociacoes() {

        let service = new NegociacaoService();

        // Executa uma lista de promessa (execução asíncrona)
        Promise.all(
            [
                service.obterNegociacaoSemana(),
                service.obterNegociacaoSemanaAnterior(),
                service.obterNegociacaoSemanaRetrasada()
            ]).then(negociacoes => {
                negociacoes
                    .reduce((newArray, array) => newArray.concat(array), [])
                    .forEach((negociacao) => this._listaNegociacao.adicionaNegociacao(negociacao))

            }).catch((error) => {
                console.log(error);
                this._mensagem.texto = error;
            });


        // Exemplo de execução de uma promise apenas

        /*let promise = service.obterNegociacaoSemana();
        promise
        .then((listaNegociacao) => {

            listaNegociacao.forEach(negociacao => {

                // Para cada negociação adiciona ela na lista da tabela
                this._listaNegociacao.adicionaNegociacao(negociacao);

            });
        })
        .catch((error) => {
            console.log(error);
        })*/

    }

    inicializaNegociacoes() {

        ConnectionFactory.getConnection()
            .then((conn) => {

                new NegociacaoDAO(conn)
                    .listaTodos()

                    .then((listaNegociacao) => {listaNegociacao.forEach((negociacao) => {

                            this._listaNegociacao.adicionaNegociacao(
                                new Negociacao(negociacao._data, negociacao._quantidade, negociacao._valor));
                        });
                    });
            });
    }


    // Funções 'privadas'

    _updateNegociacaoTableView(listaNegociacao) {

        this._negociacaoTableView.update(listaNegociacao);
    }

    _criaNegociacao() {

        return new Negociacao(
            this._data.value,
            parseInt(this._quantidade.value),
            parseFloat(this._valor.value));
    }

    _limpaFormulario() {

        this._data.value = '';
        this._quantidade.value = 0;
        this._valor.value = 0;
        this._focusForm();
    }

    _exibeMensagem(mensagem) {
        this._mensagemView.update(this._mensagem);
    }

    _focusForm() {
        this._data.focus();
    }

}