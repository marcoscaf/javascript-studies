class NegociacaoDAO {

    constructor(connection) {

        if (!connection) {
            throw new Error("Uma conexão deve ser passada");
        }

        this._connection = connection;
        this._store = 'negociacoes';

    }

    adiciona(negociacao) {

        return new Promise((resolve, reject) => {

            // Obtém a transação do tipo readwrite
            let transaction = this._connection.transaction([this._store], 'readwrite');

            // Obtém um store de negociações
            let store = transaction.objectStore(this._store)

            // Adiciona uma negociação
            let request = store.add(negociacao);

            request.onsuccess = (e) => {
                resolve();
            }

            request.onerror = (e) => {
                console.log(e.targat.error);
                reject(e.targat.error);
            }
        });

    }

    listaTodos() {

        return new Promise((resolve, reject) => {

            // Obtém a transação do tipo readwrite
            let transaction = this._connection.transaction([this._store], 'readwrite');

            // Obtém um store de negociações
            let store = transaction.objectStore(this._store)

            let cursor = store.openCursor();

            let negociacoes = [];

            cursor.onsuccess = (e) => {

                let atual = e.target.result;

                if (atual) {
                    let dado = atual.value;

                    negociacoes.push(new Negociacao(dado._data, dado._quantidade, dado._valor))

                    atual.continue();

                } else {

                    resolve(negociacoes);

                }
            }

            cursor.onerror = (e) => {
                reject(e.targat.error)
            }


        });

    }

}