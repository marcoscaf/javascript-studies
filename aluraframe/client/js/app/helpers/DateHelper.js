class DateHelper {

    constructor(){
        throw new Error('DateHelper não pode ser instanciada');
    }
   static stringToDate(dateString) {

        // Iterando sobre cada valor do split e usando uma 
        // function arrow =>, que é uma forma de criar uma função
        return new Date(...
            dateString.split('-').map((value, index) => {
                if (index == 1) {
                    value = value - 1
                }
                return value;
            }));

    }
    static datetoString(date) {
        // usando Template String `${variavel}`
        return `${date.getDate()}/${(date.getMonth() + 1)}/${date.getFullYear()}`;
    }
}