class NegociacaoTableView extends View {

    constructor(element) {
        super(element);
    }

    _template(model) {
        // Usando template String ``

        if (model.listaNegociacao.length > 0) {
            return `
                    <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>DATA</th>
                            <th>QUANTIDADE</th>
                            <th>VALOR</th>
                            <th>VOLUME</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        ${model.listaNegociacao.map(negociacao => {
                            return `
                                <tr> 
                                    <td>${DateHelper.datetoString(negociacao.data)}</td>
                                    <td>${negociacao.quantidade}</td>
                                    <td>${negociacao.valor}</td>
                                    <td>${negociacao.volume}</td>
                                </tr>
                            `
                            // Retorn um array, e para retornar uma string é só usar join
                        }).join('')}
                    </tbody>
                    
                    <tfoot>     
                        <td colspan="3">
                        </td>   
                        <td>
                            ${model.listaNegociacao.reduce((valorAtual, negociacao) => {
                            return valorAtual + negociacao.volume
                        }, 0)}
                        </td>   
                    </tfoot>
                </table>
                `
        } else {
            return `<p></p>`
        }
    }
}