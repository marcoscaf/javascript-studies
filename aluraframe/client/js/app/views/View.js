class View {
    constructor(element) {
        this._element = element;
    }
    template() {
        throw new Error('Método template precisa ser implementado na classe concreta');
    }
    update(model) {
        this._element.innerHTML = this._template(model);
    }
}
