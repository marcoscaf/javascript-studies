class MensagemView extends View {

    constructor(element) {
        super(element);
    }

    _template(model) {
        // se mensagen nao existe, retorna vazio senão a mensagem do modelo
        return model.texto ? `<p class="alert alert-info">${model.texto}</div>` : '<p></p>';
    }

}