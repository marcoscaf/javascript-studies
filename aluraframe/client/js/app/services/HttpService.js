class HttpService {

    get(path) {

        // Promisse serve para retornar uma 'Promessa' de algo que será executado no futuro (processamento assíncrono).
        // Mesma ideia do Future<Object> em Java
        return new Promise((resolve, reject) => {

            let xhr = new XMLHttpRequest();

            xhr.open('GET', path);

            xhr.onreadystatechange = () => {

                // Verifica se estado eh igual a 4 (Requisicao concluida e a resposta esta pronta) 
                // E verifica se o status da resposta eh 200 (ok)
                if (xhr.readyState == 4) {

                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.responseText));
                    } else {
                        reject(xhr.responseText);
                    }

                }
            }
            xhr.send();

        });

    }
}