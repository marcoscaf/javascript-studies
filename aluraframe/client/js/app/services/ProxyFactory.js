class ProxyFactory {

    static create(object, callBackFunction, ...props) {

        return new Proxy(object, {

            get(target, prop, receiver) {

                // Se for uma função
                if (props.includes(prop) && typeof (target[prop]) == typeof (Function)) {

                    // Metodo que será substituido adiciona ou esvazia
                    return function () {
                        console.log(`Interceptando ${prop}`);

                        // Parametro 1: função do objeto
                        // Parametro 2: contexto da execução que é o objeto passado (o this)
                        // Paramatro 3: arguments é um atributo que contém TODOS os argumentos passados pra função, 
                        //              esse arguments está disponóvel em TODOS as funções.
                        Reflect.apply(target[prop], target, arguments);

                        return callBackFunction(target);
                    }

                }
                return Reflect.get(target, prop, receiver);
            },

            set(target, prop, value, receiver) {

                if(props.includes(prop)){
                    console.log(`Interceptando ${prop}`);

                    Reflect.set(target, prop, value, receiver);
                    callBackFunction(target);

                }

                return Reflect.set(target, prop, value, receiver)

            }
        })
    }
}