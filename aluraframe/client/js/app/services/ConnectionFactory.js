
// Fazendo uso de um padrão de projetos chamado Module Pattern, que confina todo o código dentro de uma função, e exporta apenas o necessário.
// com isso as variáveis 'store', 'veresion', etc... não são acessadas de fora da classe atual.
// O retorno dessa função é a própria classe (linha 12)
var ConnectionFactory = (function () {

    const stores = ['negociacoes'];
    const version = 1;
    const dbName = 'negociacaoIndexdDB'

    var singleton = null;
    var close = null;

    return class ConnectionFactory {

        constructor() {
            throw new Error("Não é possivel criar instâncias de ConnectionFactory");
        }

        static getConnection() {
            return new Promise((resolve, reject) => {

                // abre uma conexão com o banco de dados do navegador, so o banco 'negociacaoIndexdDB' não existir ele será criado. 
                // O parametro 1 é a versão do Banco
                let openRequest = window.indexedDB.open(dbName, version);

                openRequest.onupgradeneeded = (e) => {
                    ConnectionFactory._createStores(e.target.result)
                }

                openRequest.onsuccess = (e) => {

                    // Se a conexão for nula, inicializa, se não for nula, retorna ela mesmo. 
                    // Isso serve para evitar que muitas conexões sejam abertas
                    if(!singleton){
                        singleton = e.target.result;


                        // Fazendo aqui um Monkey Patch para mudar o comportamento atual do close.
                        // Para evitar que o desenvolvedor chame diretamente o close de connection 
                        // e podendo correr o risco de nas próximas vezes obter uma conexão fechada.

                        // Antes de mudar a função close, vamos salvar o original na variável 'close', 
                        // o bind(ongleton) serve para dizer que o contexto (this) do close é o singleton, 
                        // isso é necessário pois sem esse bind, a função close estaria sozinha sem um contexto
                        close = singleton.close.bind(singleton);

                        singleton.close= function(){
                            throw new Error("Não é possível fechar a conexão diretamente, use a função ConnectionFactory.closeConnection()")
                        }
                    }
                    resolve(singleton)
                }

                openRequest.onerror = (e) => {
                    reject(singleton);
                }

            })
        }

        static _createStores(connection) {
            stores.forEach((store) => {

                // Verifica se o banco existe, se sim, ele será apagado e recriado.
                if (connection.objectStoreNames.contains(store)) {
                    connection.deleteObjectStore(store);
                }

                // Cria o banco
                connection.createObjectStore(store, { autoIncrement: true })

            });
        }

        static closeConnection(){
            if(singleton){
                close();
                singleton = null;
            }
        }
    }
})
// Chama a função anonima recém criada na linha 4
();