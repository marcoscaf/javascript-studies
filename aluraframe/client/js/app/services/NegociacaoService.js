class NegociacaoService {

    constructor() {
        this._httpService = new HttpService();
    }

    obterNegociacaoSemana() {

        // Promisse serve para retornar uma 'Promessa' de algo que será executado no futuro (processamento assíncrono).
        // Mesma ideia do Future<Object> em Java
        return new Promise((resolve, reject) => {

            let promise = this._httpService.get('/negociacoes/semana');

            promise
                .then(listaNegociacao => {
                    resolve(listaNegociacao.map(object => new Negociacao(new Date(object.data), object.quantidade, object.valor)))})
                .catch(error => reject(error))
        });
    }

    obterNegociacaoSemanaAnterior() {

        // Promisse serve para retornar uma 'Promessa' de algo que será executado no futuro (processamento assíncrono).
        // Mesma ideia do Future<Object> em Java
        return new Promise((resolve, reject) => {

            let promise = this._httpService.get('/negociacoes/anterior');

            promise
                .then(listaNegociacao => {
                    resolve(listaNegociacao.map(object => new Negociacao(new Date(object.data), object.quantidade, object.valor)))})
                .catch(error => reject(error))
        });
    }

    obterNegociacaoSemanaRetrasada() {

        // Promisse serve para retornar uma 'Promessa' de algo que será executado no futuro (processamento assíncrono).
        // Mesma ideia do Future<Object>  em Java
        return new Promise((resolve, reject) => {

            let promise = this._httpService.get('/negociacoes/retrasada');

            promise
                .then(listaNegociacao => {
                    resolve(listaNegociacao.map(object => new Negociacao(new Date(object.data), object.quantidade, object.valor)))})
                .catch(error => reject(error))
        });
    }
}