class ListaNegociacao{

    constructor(){
        this._listaNegociacao = [];
    }

    adicionaNegociacao(negociacao){
        this._listaNegociacao.push(negociacao);
    }

    get listaNegociacao(){
        // Programação defensiva,
        // retornando uma nova instância da ista
        return [].concat(this._listaNegociacao);
    }

    esvazia(){
        this._listaNegociacao = [];
    }
}