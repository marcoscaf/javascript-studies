class Negociacao {

    // O javascrip não tem modificadores de acesso, então, 
    // por convenção atributos que iniciam com _ (underline), 
    // não deveriam ser acessados de fora
    constructor(data, quantidade, valor) {
        this._data = new Date(data);
        this._quantidade = quantidade;
        this._valor = valor;

        // Congela a instância, evita alterações externas
        Object.freeze(this);
    }

    get volume() {
        return this._quantidade * this._valor;
    }

    get data(){
        new Date()
        return new Date(this._data); 
    }
    get quantidade(){
        return parseInt(this._quantidade);
    }
    get valor(){
        return parseFloat(this._valor);
    }
}